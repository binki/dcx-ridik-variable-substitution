﻿/* -*- indent-tabs-mode: nil; -*-
 * Copyright (c) 2014, Direct ConneX, LLC
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Direct ConneX, LLC nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
'use strict';

var _, Drupal, jQuery, module, require, Ridik;

if (!_ && require)
    _ = require('lodash');

(function () {
    Ridik = Ridik || {};
    if ((module || {}).exports)
        module.exports = Ridik;

    /* Replace with calls to Drupal.checkPlain()? */
    var ENT_QUOTES = 1;
    function htmlentities(text, options)
    {
        if (options === undefined)
            options = 0;
        text = new String(text).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        if (options & ENT_QUOTES)
            text = text.replace(/"/g, '&quot;').replace(/'/g, '&apos;');
        return text;
    }

    /**
     * \brief
     *   Ridiculous variable substitution.
     *
     * A variable substitution does a special type of evaluation on
     * any expression enclosed by ${}.
     *
     * <template> ::= "${" <expression>"}"
     *
     * <expression> ::= "(" <expression> ")" | <whitespace> <expression> | <expression> <whitespace> | <ternary> | <expression> <filter> | <literal> | <variable> | <expression> <expression>
     *
     * <filter> ::= "|" <filter_spec>
     *
     * <filter_spec> ::= <filter_simple_name> | <filter_sed_spec> | <filter_item_spec>
     *
     * <filter_simple_name> ::= "h" | "k" | "u"
     *
     * <filter_sed_spec> ::= "s" <argument_separator> <expression> <argument_separator> <expression> <argument_separator> <expression>
     *
     * <filter_item_spec> ::= "i" <argument_separator> <expression>
     *
     * <argument_separator> ::= ","
     *
     * <literal> ::= '"' {<char_not_quote, backslash escapes for backslash and doublequote>, …} '"' | "'" {<char_not_quote, blackslash escapes for backslahs and singlequote> "'"
     *
     * <variable> ::= <identifier> | <identifier> "." <variable>
     *
     * <identifier> ::= [a-zA-Z_][a-zA-Z0-9_]*
     *
     * <ternary> ::= <expression> "?" <expression> ":" <expression>
     *
     * \param variables
     *   Plain object whose contents are referenced by the variable
     *   expressions.
     * \param context
     *   The DOMElement, Document, or jQuery whose descendents should
     *   be searched for elements with the variable-substitution class
     *   upon which to perform substitution.
     */
    function variables_substitute(variables, context) {
        jQuery('.variable-substitution', context).each(function(i, e) {
            var o = jQuery(e);
            o.text(variables_substitute_string(o.data('variable_substitution'), variables));
        });
        jQuery('.variable-substitution-raw', context).each(function(i, e) {
            var o = jQuery(e);
            o.html(variables_substitute_string(o.data('variable_substitution'), variables));
        });
        jQuery('.variable-value-substitution', context).each(function(i, e) {
            var o = jQuery(e);
            o.val(variables_substitute_string(o.data('variable_value_substitution'), variables));
        });
    }
    Ridik.variables_substitute = variables_substitute;

    /**
     * \brief
     *   Prepare substituted elements so that
     *   variables_substitute_ready() could be called on them again.
     *
     * Prepeares the substitutable objects within context so that they
     * can be shallowly-cloned and reinserteed into the document and
     * then have variables_substitue_ready() called on them. Without
     * unsubstituting the elements first, the currently-substituted
     * values would be read as the template values by a future
     * variables_substitute_ready()
     * call. (variables_substitute_ready() always looks at the current
     * values of elements when storing the templates to a backing to
     * support hypothetical usecase that the template itself be
     * dynamically updated).
     *
     * \returns
     *   context to enable chaaining.
     */
    function variables_unsubstitute(context) {
        jQuery('.variable-substitution', context).each(function(i, e) {
            var o = jQuery(e);
            o.text(o.data('variable_substitution'));
        });
        jQuery('.variable-substitution-raw', context).each(function(i, e) {
            var o = jQuery(e);
            o.text(o.data('variable_substitution'));
        });
        jQuery('.variable-value-substitution', context).each(function(i, e) {
            var o = jQuery(e);
            o.val(o.data('variable_value_substitution'));
        });
        return context;
    }
    Ridik.variables_unsubstitute = variables_unsubstitute;

    function variables_substitute_string(s, variables) {
        /*
         * Right now, this does not enable literals in a template to
         * contain ‘}’. This should be altered to be like POSIX sh’s
         * “$()” syntax—the parsing of all characters following a “${”
         * should be done by the tokenizer and the terminating ‘}’
         * seen as a special token (like how parentheses are handled
         * specially by the tokentreeizer).
         */
        return s.replace(/\${([^}]*)}/g, function(match, p1) {
            return variables_substitute_expression(variables_substitute_tokentreeize(p1), variables);
        });
    }
    Ridik.variables_substitute_string = variables_substitute_string;

    function variables_substitute_tokentreeize(s)
    {
        /* overkill! */
        var tree = {tokens: []};
        var things = {
            whitespace: /^(\s+)/, /* <whitespace> */
            literal: { /* <literal> */
                exec: function (s) {
                    /*
                     * Implement the string parser as a small state
                     * machine because my non-greedy regex liked to
                     * http://imgur.com/0twrqMQ
                     */
                    var quote = (/^['"]/.exec(s) || [])[0];
                    if (!quote)
                        return null;

                    var i = 1;
                    while (i < s.length)
                    {
                        if (s[i] === quote)
                            return [null, s.substring(0, i+1)];

                        if (s[i] === '\\')
                        {
                            i++;
                            /*
                             * Accept an escaped backslash or an
                             * escaped quote, anything else is an
                             * error.
                             */
                            if (i < s.length
                                && (quote + '\\').indexOf(s[i]) == -1)
                                /*
                                 * Is there a better way to inform the
                                 * user that an invalid escape
                                 * sequence was detected? For long
                                 * strings, just failing generally
                                 * like this will almost make the
                                 * error impossible to find…
                                 */
                                return null;
                        }

                        i++;
                    }
                    return null;
                }
            },
            /* http://www.unicode.org/reports/tr31/#Alternative_Identifier_Syntax */
            identifier: /^([^0-9\s-,.<>?[\]{}\\\/|`~!@#$%^&*();:'"][^\s-,.<>?[\]{}\\\/|`~!@#$%^&*();:'"]*)/, /* <identifier> */
            dot: /^(\.)/, /* "." */
            comma: /^(,)/, /* "," */
            lparen: /^(\()/, /* "(" */
            rparen: /^(\))/, /* ")" */
            question: /^(\?)/, /* "?" */
            colon: /^(:)/, /* ":" */
            pipe: /^(\|)/ /* "|" */
        };
        /*
         * If supporting operations like multiplication and order of
         * operations, do no try to tree those out here. Instead,
         * retree the existing tree after tokenization. Parens are
         * handled here as an optimization/shortcut.
         */
        var treeactions = {
            'default': function (tree, type, data) {
                tree.tokens.push({type: type, data: data});
                return tree;
            },
            lparen: function (tree) {
                var newtree = {type: 'expression', data: '<uncalculated>', parent: tree, tokens: []};
                tree.tokens.push(newtree);
                return newtree;
            },
            rparen: function (tree) {
                var p = tree.parent;
                delete tree.parent;
                return p;
            }
        };
        var last_length = -1;
        while (s.length)
        {
            if (s.length == last_length)
                return {tokens:[{type: 'literal', data: 'BUG: infinite loop detected in ridik-variable-substitution.'}]};
            last_length = s.length;

            var matches;
            var type;
            for (type in things)
                if ((matches = things[type].exec(s)))
                    break;
            if (matches)
                tree = (treeactions[type] || treeactions['default'])(tree, type, matches[1]);
            else
                return {tokens:[{type: 'literal', data: '"unrecognized token near “' + htmlentities(s, ENT_QUOTES) + '”'}]};
            /*
             * Check if tokens is now undefined (encountered
             * extraneous closing parenthesis causing erroneous
             * treeactions.rparen() call) and report error if so.
             */
            if (!tree)
                return {tokens:[{type: 'literal', data: '"extraneous closing parenthesis near ' + htmlentities(s, ENT_QUOTES) + '"'}]};
            /* Advance past this token. */
            s = s.substring(matches[1].length);
        }
        return tree;
    }

    function variables_substitute_tree_split(tree, i)
    {
        return {
            left: _.assign({}, tree, {tokens: tree.tokens.slice(0, i)}),
            right: _.assign({}, tree, {tokens: tree.tokens.slice(i)})
        };
    }

    function variables_substitute_argument_list(tree)
    {
        var args = [];
        var i;
        while (tree.tokens.length)
        {
            for (i = 0; i < tree.tokens.length && tree.tokens[i].type !== 'comma'; i++)
                ;
            tree = variables_substitute_tree_split(tree, i);
            args.push(tree.left);
            tree = tree.right;
            if (tree.tokens.length == 1)
                args.push({tokens:[{type: 'literal', data: 'hanging comma at end of argument list'}]});
            tree = variables_substitute_tree_split(tree, 1).right;
        }
        return args;
    }

    function variables_substitute_boolize(s)
    {
        /*
         * Our system treats false/undefined/'' as identical. To work
         * nicely, we would have comparison operators at the very
         * least and types instead of just strings.
         */
        return !!s /* JavaScript truth value: '' is false, undefined is false, null is false */;
    }

    function variables_substitute_expression(tree, variables)
    {
        /*
         * Decide which BNF to use based on what the next token looks
         * like.
         */
        /* No tokens — no expression */
        if (!tree.tokens.length)
            return 'no tokens';

        /* <whitespace> <expression> */
        if (tree.tokens[0].type === 'whitespace')
            return variables_substitute_expression(variables_substitute_tree_split(tree, 1).right, variables);
        /* <expression> <whitespace> */
        if (tree.tokens[tree.tokens.length - 1].type === 'whitespace')
            return variables_substitute_expression(variables_substitute_tree_split(tree, -1).left, variables);

        var i;
        /* <expression> ?????? */
        /*
         * Evaluate from right to left to allow filter chaining. Also
         * has ternary eat *everything* to its left up, just like the
         * real (JavaScript) ternary operator likes to.
         */
        for (i = tree.tokens.length - 1; i > 0; i--)
        {
            switch (tree.tokens[i].type)
            {
                /* <ternary> ::= <expression> "?" <expression> ":" <expression> */
            case 'question':
                var ternary_question = variables_substitute_tree_split(tree, i);
                /* Drop the 'question' token */
                ternary_question.right.tokens.shift();
                /* Search for the last colon */
                for (i = ternary_question.right.tokens.length - 1; i >= 0; i--)
                    if (ternary_question.right.tokens[i].type === 'colon')
                        break;
                if (i < 0)
                    return 'ternary operator missing colon';
                var ternary_answer = variables_substitute_tree_split(ternary_question.right, i);
                /* Drop the 'colon' token. */
                ternary_answer.right.tokens.shift();
                return variables_substitute_expression(
                    ternary_answer[variables_substitute_boolize(variables_substitute_expression(ternary_question.left, variables)) ? 'left' : 'right'],
                    variables);

                /* <expression> <filter> */
            case 'pipe':
                if ((i + 1) >= tree.tokens.length)
                    return 'Expecting tokens after ‘|’ (pipe).';
                if (tree.tokens[i + 1].type !== 'identifier')
                    return 'Expecting filter identifier after ‘|’ (pipe) near “' + tree.tokens[i + 1].data + '”.';
                var filter_split = variables_substitute_tree_split(tree, i);
                var filter_expect_just_identifier = function (value, tree) {
                    return tree.tokens.length > 2 ? 'invalid filter specification: ' + tree.tokens[0].data + ': there were tokens after the filter specification near “' + tree.tokens[2].data + '”' : value;
                };
                return (
                    ({
                        /* htmlentities() */
                        h: function (s, tree) {
                            return filter_expect_just_identifier(htmlentities(s, ENT_QUOTES), tree);
                        },
                        /* item */
                        i: function (s, tree) {
                            /* Ensure comma then interpret argument list after that comma. */
                            if (tree.tokens.length < 3)
                                return 'invalid filter specification: i: end of expression while expecting comma';
                            if (tree.tokens[2].type !== 'comma')
                                return 'invalid filter specification: i: first token after ‘i’ is not comma, expecting comma near “' + String(tree.tokens[2].data) + '”';
                            tree = variables_substitute_tree_split(tree, 3).right;

                            var args = variables_substitute_argument_list(tree);
                            if (args.length !== 1)
                                return 'invalid filter specification: i: expects 1 argument instead of ' + args.length;
                            args = _.map(args, function (arg) {
                                return variables_substitute_expression(arg, variables);
                            });

                            var o;
                            try
                            {
                                o = JSON.parse(s);
                            }
                            catch (e)
                            {
                                o = e;
                            }
                            if (o === null || o === undefined)
                                return '';
                            return variables_substitute_stringify(o[args[0]]);
                        },
                        /* keys */
                        k: function (s, tree) {
                            var o;
                            try
                            {
                                o = JSON.parse(s);
                            }
                            catch (e)
                            {
                                o = e;
                            }
                            return filter_expect_just_identifier(
                                JSON.stringify(
                                    _.map(
                                        o || [],
                                        function (e, i) {
                                            return i;
                                        })), tree);
                        },
                        /* sed */
                        s: function (s, tree, variables) {
                            /*
                             * Ensure comma directly after ‘s’. Then treat
                             * everything after that comma as an argument
                             * list.
                             */
                            if (tree.tokens.length < 3)
                                return 'invalid filter specification: s: end of expression while expecting comma';
                            if (tree.tokens[2].type !== 'comma')
                                return 'invalid filter specification: s: first token after ‘s’ is not comma, expecting comma near “' + String(tree.tokens[2].data) + '”';
                            tree = variables_substitute_tree_split(tree, 3).right;

                            var args = variables_substitute_argument_list(tree);
                            if (args.length !== 3)
                                return 'invalid filter specification: s: expects 3 arguments instead of ' + args.length;
                            args = _.map(args, function (arg) {
                                return variables_substitute_expression(arg, variables);
                            });

                            var regex;
                            try
                            {
                                /*
                                 * Not sure second arg of RegExp() may be
                                 * undefined or '', so just don’t pass it
                                 * when it’s ''.
                                 */
                                if (args[2])
                                    regex = new RegExp(args[0], args[2]);
                                else
                                    regex = new RegExp(args[0]);
                            }
                            catch (ex)
                            {
                                return 'invalid filter specification: s: regex uncompilable: ' + String(ex.message);
                            }
                            return s.replace(regex, args[1]);
                        },
                        /* rawurlencode() */
                        u: function (s, tree) {
                            return filter_expect_just_identifier(encodeURIComponent(s), tree);
                        }
                    })[filter_split.right.tokens[1].data] || function (s) {
                        return 'invalid filter specification: ' + filter_split.right.tokens[1].data;
                    })(variables_substitute_expression(filter_split.left, variables), filter_split.right, variables);
            }
        }

        var s = undefined;
        /* "(" <expression> ")" */
        if (tree.tokens[0].type === 'expression')
        {
            s = variables_substitute_expression(tree.tokens[0], variables);
            tree = variables_substitute_tree_split(tree, 1).right;
        }
        /* <literal> */
        else if (tree.tokens[0].type === 'literal')
        {
            s = tree.tokens[0].data.substring(1, tree.tokens[0].data.length - 1).replace(/\\(.)/g, '$1');
            tree = variables_substitute_tree_split(tree, 1).right;
        }
        /* <variable> */
        else if (tree.tokens[0].type === 'identifier')
        {
            var ptr = variables;
            /*
             * Accumulator allows variable named 'a.b' to be fulfilled
             * by both {'a.b': z} and {a: {b: z}}.
             */
            var varname_accumulator = '';
            for (i = 0; i < tree.tokens.length; i++)
            {
                /* odd index has a dot */
                if ((i % 2) == 1)
                {
                    if (tree.tokens[i].type !== 'dot')
                        /* end of <variable> */
                        break;
                    if (varname_accumulator.length)
                        varname_accumulator += tree.tokens[i].data;
                    /* dot _must_ be followed by an identifier */
                    i++;
                    if (i >= tree.tokens.length)
                        return 'expected identifier but reached end of tokens instead';
                }
                /* even index has an identifier */
                if (tree.tokens[i].type !== 'identifier')
                    return 'expected identifier instead of ' + tree.tokens[i].type;
                varname_accumulator += tree.tokens[i].data;
                if (ptr !== undefined && ptr !== null
                    && ptr[varname_accumulator] !== undefined)
                {
                    ptr = ptr[varname_accumulator];
                    varname_accumulator = '';
                }
            }
            /*
             * If there is something left in varname_accumulator, that
             * means the data ultimately was missing.
             */
            if (varname_accumulator)
                ptr = undefined;
            s = variables_substitute_stringify(ptr);
            tree = variables_substitute_tree_split(tree, i).right;
        }
        else
            return 'unexpected token type ' + tree.tokens[0].type;

        /* <expression> <expression> (concatenation) */
        if (tree.tokens.length)
            s += variables_substitute_expression(tree, variables);

        return s;
    }

    /**
     * \brief
     *   Render an object to string.
     *
     * This will render string and number to their default string
     * values, plain object and array to JSON, and anything else
     * (e.g., undefined) to '' (useful for ternaries). This exists
     * because there are multiple places where unidentified flying
     * objects need to be coerced to string value. Namely, the direct
     * identifier reference syntax and the “|i,key” syntax. There’s a
     * possibility that covers everything… ;-).
     *
     * \param o
     *   The object to render.
     */
    function variables_substitute_stringify(o) {
        if (typeof(o) === 'string'
            || o === true
            || typeof(o) === 'number')
            return String(o);
        else if (_.isArray(o)
                 || _.isPlainObject(o))
            return JSON.stringify(o);
        else
            return '';
    }

    /**
     * \brief
     *   Prepare elements marked with the variable-substitution and
     *   variable-value-substitution classes.
     *
     * Stores each element’s variable substitution template so that it
     * can be re-expanded later. Since the templates are expanded
     * “in-place”, they must be copied out so that expansion can be
     * done over and over.
     *
     * This must be done as new content is introduced into the DOM. If
     * using Drupal.attachBehaviors(), this will be automatically
     * taken care of for you.
     *
     * \param context
     *   The context in which to process content—either a DOM element
     *   or jQuery object constraining the range or undefined to apply
     *   to the whole document.
     */
    function variables_substitute_ready(context) {
        jQuery('.variable-substitution, .variable-substitution-raw', context).each(function(i, e) {
            var o = jQuery(e);
            o.data('variable_substitution', o.text());
        });
        jQuery('.variable-value-substitution', context).each(function(i, e) {
            /*
             * Ensure that we’re reading the template and not
             * user-provided autocomplete data. Once I had this
             * confusing problem where mozilla had remembered the
             * input’s value from before and I couldn’t tell where the
             * value was coming from and the autocomplete happens
             * before jQuery(document).ready() is called, so it would
             * have been difficult or impossible to intercept
             * it. defaultValue will have the original value.
             */
            var o = jQuery(e);
            var value = o.attr('value');
            if (value === undefined || value === null)
                console.log('Element using variable-value-substitution without a value.');
            o.data('variable_value_substitution', value);
        });
    }
    Ridik.variables_substitute_ready = variables_substitute_ready;

    /*
     * Maybe external scripts aren’t supposed to do this. Huh.
     */
    if ((Drupal || {}).behaviors)
        Drupal.behaviors.Ridik_variable_substitution = {
            attach: function (context, settings) {
                variables_substitute_ready(context);
            },
            detach: function (context, settings, trigger) {
                variables_unsubstitute(context);
                jQuery('.variable-substitution, .variable-substitution-raw, .variable-value-substitution', context).removeData([
                    'variable_substitution',
                    'variable_value_substitution'
                ]);
            }
        };
})();
